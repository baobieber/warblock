﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BlockController : MonoBehaviour
{
    public ColorGunNBullet color;
    public int maxHealth;
    Image image;
    float speed;
    public bool isMarker;
    float yTrigger;
    bool triggerToSpawn;
    [SerializeField]
    int health;

    private void Awake()
    {
        speed = GameController.Instance.speedDropDown;
        if (isMarker)
        {
            yTrigger = ResponsiveController.Instance.height - ResponsiveController.Instance.ConvertHeight( 120 / 2);
        }
        image = GetComponent<Image>();
    }

    private void Start()
    {
        StartCoroutine(DropDown());
        if (isMarker)
        {
            StartCoroutine(CheckToSpawnNewLine());
        }
        health = maxHealth;
    }

    IEnumerator DropDown()
    {
        while (true)
        {
            Vector3 v = transform.position;
            transform.position = new Vector3(v.x, v.y - ResponsiveController.Instance.ConvertHeight(1 * speed), v.z);
            yield return new WaitForSeconds(0.01f);
        }
    }

    IEnumerator CheckToSpawnNewLine()
    {
        while (!triggerToSpawn)
        {
            yield return new WaitForEndOfFrame();

            if (transform.position.y <= yTrigger)
            {
                triggerToSpawn = true;
                GameController.Instance.SpawnDiamondLine();
                //Debug.Log("Spawn");
            }
        }
       
    }

    private void Update()
    {
        if (!isMarker)
        {
            if (transform.position.y < 0)
            {
                GameController.Instance.DestroyDiamondRemoveFromList(gameObject);
            }
        }
        else
        {
            if (transform.position.y < 0)
            {
                Destroy(gameObject);
            }
        }
    }

    public void TakeDamage(int damage)
    {
        Debug.Log("A: " + image.color.a);
        if (health - damage > 0)
        {
            health -= damage;
            image.color = new Color( image.color.r, image.color.b, image.color.g , ((float)health / maxHealth));
        }
        else
        {
            DestroyBlock();
        }
    }

    public void DestroyBlock()
    {
        GameController.Instance.DestroyDiamondRemoveFromList(gameObject);
    }
}
