﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public ColorGunNBullet color;
    public int damage;
    float speed;
    RectTransform rectTransform;
    int countTimeToLift;
    public int column;
    GameObject diamondTarget;
    float yTarget;
    public bool hasTarget;

    private void Awake()
    {
        speed = GameController.Instance.speedFlyUp;
        rectTransform = GetComponent<RectTransform>();
    }

    private void FixedUpdate()
    {
        countTimeToLift++;

        foreach (GameObject each in GameController.Instance.listDiamondColumnManager[column])
        {
            if (each.CompareTag("Diamond"))
            {
                diamondTarget = each;
                break;
            }
        }

        if (diamondTarget == null)
        {
            diamondTarget = GameController.Instance.listDiamondStatic[column];
        }

        yTarget = ResponsiveController.Instance.ReConvertHeight(diamondTarget.transform.position.y);
        if (rectTransform.offsetMax.y + speed < yTarget)
        {
            //Debug.Log("offsetMax: " + rectTransform.offsetMax.y + " posY: " + yTarget);
            rectTransform.offsetMax = new Vector2(rectTransform.offsetMax.x, rectTransform.offsetMax.y + speed);
        }
        else
        {
            rectTransform.offsetMax = new Vector2(rectTransform.offsetMax.x, yTarget);
            rectTransform.offsetMin = new Vector2(rectTransform.offsetMin.x, rectTransform.offsetMin.y + speed);
            if (rectTransform.offsetMin.y >= yTarget)
            {
                if (hasTarget)
                {
                    //Fly Up To Target
                    Destroy(gameObject);
                    if (CheckColorMatchSense(diamondTarget.GetComponent<BlockController>()))
                    {
                        diamondTarget.GetComponent<BlockController>().DestroyBlock();
                        GameController.Instance.DestroyDiamondRemoveFromList(diamondTarget);
                    }
                    else
                    {
                        diamondTarget.GetComponent<BlockController>().TakeDamage(damage);
                    }
                }
                else
                {
                    //Fly Up To Void
                }
            }
        }

        if (rectTransform.offsetMin.y > ResponsiveController.Instance.height + (ResponsiveController.Instance.height * 80) / 100)
        {
            Destroy(gameObject);
        }
    }

    bool CheckColorMatchSense(BlockController blockController)
    {
        Debug.Log("block: " + blockController.color + " bullet: " + color);
        if (color.Equals(blockController.color))
        {
            return true;
        }
        else
        {
            return false;
        }

    }

}
