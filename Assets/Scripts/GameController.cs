﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

public enum ColorGunNBullet
{
    Green, Purple, Red, Sky, Yellow
}

public class GameController : MonoBehaviour
{
    #region Public Var
    static public GameController Instance;
    [Header("Public")]
    public GameObject prefabDiamond;
    public GameObject prefabDiamondMarker;
    public Transform diamondGroup;
    public List<Transform> listSpawnDiamond = new List<Transform>();
    public List<Sprite> listColorDiamond = new List<Sprite>();
    public List<Sprite> listColorGun = new List<Sprite>();
    public List<Sprite> listColorEye = new List<Sprite>();
    public Image currentColorGunImage;
    public Image nextColorGunImage;
    public Image eyeColorImage;

    public float speedDropDown;
    public float speedFlyUp;

    public GameObject bulletPrefab;
    public Transform bulletGroup;

    public List<Transform> listSpawnBullet = new List<Transform>();
    public List<List<GameObject>> listDiamondColumnManager = new List<List<GameObject>>();

    public List<Sprite> listBulletSprite = new List<Sprite>();
    public float timeToChangeColorGun;
    public float smoothTimeBulletSprite;

    public Image timeBulletImage;
    public List<Sprite> listTimeBulletSprite = new List<Sprite>();

    public List<GameObject> listDiamondStatic = new List<GameObject>();
    #endregion

    #region Private Var
    List<GameObject> listDiamond = new List<GameObject>();
    [Header("Private")]
    [SerializeField]
    ColorGunNBullet currentColorGun;
    [SerializeField]
    ColorGunNBullet nextColorGun;

    [SerializeField]
    List<GameObject> listDiamondColumn_0 = new List<GameObject>();
    [SerializeField]
    List<GameObject> listDiamondColumn_1 = new List<GameObject>();
    [SerializeField]
    List<GameObject> listDiamondColumn_2 = new List<GameObject>();
    [SerializeField]
    List<GameObject> listDiamondColumn_3 = new List<GameObject>();
    [SerializeField]
    List<GameObject> listDiamondColumn_4 = new List<GameObject>();
    [SerializeField]
    List<GameObject> listDiamondColumn_5 = new List<GameObject>();

    bool waveOver;

    BulletController bulletControllerBulletPrefab;
    Image imageOfBulletPrefab;

    BlockController blockControllerDiamondPrefab;

    #endregion

    #region Callbacks   
    private void Awake()
    {
        Instance = this;
        listDiamondColumnManager.Add(listDiamondColumn_0);
        listDiamondColumnManager.Add(listDiamondColumn_1);
        listDiamondColumnManager.Add(listDiamondColumn_2);
        listDiamondColumnManager.Add(listDiamondColumn_3);
        listDiamondColumnManager.Add(listDiamondColumn_4);
        listDiamondColumnManager.Add(listDiamondColumn_5);

        bulletControllerBulletPrefab = bulletPrefab.GetComponent<BulletController>();
        imageOfBulletPrefab = bulletPrefab.GetComponent<Image>();

        blockControllerDiamondPrefab = prefabDiamond.GetComponent<BlockController>();
    }

    private void Start()
    {
        SpawnDiamondLine();
        StartCoroutine(StartChangeColorGun());
    }
    #endregion

    #region Diamond

    void SpawnDiamond(Vector3 pos, int colorIndex, int index)
    {
        blockControllerDiamondPrefab.color = (ColorGunNBullet)colorIndex;
        GameObject diamond = Instantiate(prefabDiamond, pos, Quaternion.identity, diamondGroup);
        diamond.GetComponent<Image>().sprite = listColorDiamond[colorIndex];
        AddDiamondToListColumn(index, diamond);
        listDiamond.Add(diamond);
    }

    void SpawnDiamondMarker(Vector3 pos)
    {
        GameObject diamondMarker = Instantiate(prefabDiamondMarker, pos, Quaternion.identity, diamondGroup);
        //AddDiamondToListColumn(0, diamondMarker);
        //listDiamond.Add(diamondMarker);
    }

    public void SpawnDiamondLine()
    {
        int randSpawn;
        int randColor;

        SpawnDiamondMarker(listSpawnDiamond[0].position);

        for (int i = 0; i < listSpawnDiamond.Count; i++)
        {
            randSpawn = Random.Range(0, 2);
            if (randSpawn == 1)
            {
                randColor = Random.Range(0, listColorDiamond.Count);
                SpawnDiamond(listSpawnDiamond[i].position, randColor, i);
            }
        }
    }

    void AddDiamondToListColumn(int index, GameObject diamond)
    {
        listDiamondColumnManager[index].Add(diamond);
    }

    public void DestroyDiamondRemoveFromList(GameObject diamond)
    {

        foreach (List<GameObject> each in listDiamondColumnManager)
        {
            if (each.Contains(diamond))
            {
                each.Remove(diamond);
                Destroy(diamond);
                return;
            }
        }
        
    }
    #endregion

    #region Gun
    IEnumerator StartChangeColorGun()
    {
        float countTime = 0;
        while (true)
        {
            if (smoothTimeBulletSprite <= 0)
            {
                yield return new WaitForSeconds(1 / smoothTimeBulletSprite);
                Debug.Log("SMOOTH ERRORRRRRRRRRRRRRRRRRRRRRRRRRRRRRR!!!!!!!!!!!!!!!!!!!!!!!!");
            }
            else
            {
                yield return new WaitForSeconds(1 / smoothTimeBulletSprite);
                countTime += 1 / smoothTimeBulletSprite;
                ChangeFillTimeBulletImage(countTime);
                if (countTime >= timeToChangeColorGun)
                {
                    yield return new WaitForSeconds(0.5f);
                    countTime = 0;
                    ChangeColorGun();
                    ChangeTimeBulletSprite();
                }
            }
        }
    }

    public void ChangeColorGun()
    {
        int randNextColorGun = Random.Range(0, Enum.GetNames(typeof(ColorGunNBullet)).Length);
        currentColorGun = nextColorGun;
        currentColorGunImage.sprite = listColorGun[(int)currentColorGun];
        eyeColorImage.sprite = listColorEye[(int)currentColorGun];
        nextColorGun = (ColorGunNBullet)randNextColorGun;
        nextColorGunImage.sprite = listColorGun[(int)nextColorGun];
    }

    void ChangeTimeBulletSprite()
    {
        timeBulletImage.sprite = listTimeBulletSprite[(int)currentColorGun];
        timeBulletImage.fillAmount = 0;
    }

    void ChangeFillTimeBulletImage(float currentTime)
    {
        float percent = currentTime / timeToChangeColorGun;
        //Debug.Log("Percent: " + percent);
        timeBulletImage.fillAmount = percent;
    }
    #endregion

    #region Bullet
    public void Shoot(int index)
    {
        if (listDiamondColumnManager[index].Count > 0)
        {
            bulletControllerBulletPrefab.hasTarget = true;
        }
        else
        {
            bulletControllerBulletPrefab.hasTarget = false;
        }
        bulletControllerBulletPrefab.column = index;
        bulletControllerBulletPrefab.damage = 1;
        bulletControllerBulletPrefab.color = currentColorGun;
        imageOfBulletPrefab.sprite = listBulletSprite[(int)currentColorGun];
        GameObject bullet = Instantiate(bulletPrefab, listSpawnBullet[index].position, Quaternion.identity, bulletGroup);
    }
    #endregion
}
