﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResponsiveController : MonoBehaviour
{
    static public ResponsiveController Instance;

    public float width;
    public float height;

    public float defaultWidth = 720;
    public float defaultHeight = 1280;

    private void Awake()
    {
        Instance = this;

        width = Screen.width;
        height = Screen.height;
    }

    public Vector3 ConvertVector3(Vector3 vector3)
    {
        float xRes = (vector3.x * width) / defaultWidth;
        float yRes = (vector3.y * height) / defaultHeight;

        return new Vector3(xRes, yRes, vector3.z);
    }

    public float ConvertHeight(float value)
    {
        return ((value * height) / defaultHeight);
    }

    public float ReConvertHeight(float value)
    {
        return ((value * defaultHeight) / height);
    }

    public float ConvertWidth(float value)
    {
        return ((value * width) / defaultWidth);
    }
}
